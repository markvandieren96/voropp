#include "interface.hpp"

using namespace voro;

std::unique_ptr<container> voro::container_ctor(double ax_, double bx_, double ay_, double by_, double az_, double bz_,
												int nx_, int ny_, int nz_, bool xperiodic_, bool yperiodic_, bool zperiodic_, int init_mem)
{
	return std::make_unique<container>(
		ax_, bx_, ay_, by_, az_, bz_,
		nx_, ny_, nz_, xperiodic_, yperiodic_, zperiodic_, init_mem);
}

void voro::container_draw_particles(container &container, rust::Str filename)
{
	container.draw_particles(((std::string)(filename)).c_str());
}
