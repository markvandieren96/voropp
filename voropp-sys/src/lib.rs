pub use cxx;

#[cxx::bridge]
pub mod ffi {
	#[namespace = "voro"]
	unsafe extern "C++" {
		include!("voro++.hh");
		include!("interface.hpp");

		type container;
		fn container_ctor(
			ax_: f64,
			bx_: f64,
			ay_: f64,
			by_: f64,
			az_: f64,
			bz_: f64,
			nx_: i32,
			ny_: i32,
			nz_: i32,
			xperiodic_: bool,
			yperiodic_: bool,
			zperiodic_: bool,
			init_mem: i32,
		) -> UniquePtr<container>;

		fn put(self: Pin<&mut container>, n: i32, x: f64, y: f64, z: f64);

		fn sum_cell_volumes(self: Pin<&mut container>) -> f64;

		fn find_voronoi_cell(
			self: Pin<&mut container>,
			x: f64,
			y: f64,
			z: f64,
			rx: &mut f64,
			ry: &mut f64,
			rz: &mut f64,
			pid: &mut i32,
		) -> bool;

		fn container_draw_particles(container: Pin<&mut container>, path: &str);
	}
}
