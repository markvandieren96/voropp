#pragma once

#include <memory>
#include "voro++.hh"
#include "rust/cxx.h"

namespace voro
{
	std::unique_ptr<container> container_ctor(double ax_, double bx_, double ay_, double by_, double az_, double bz_,
											  int nx_, int ny_, int nz_, bool xperiodic_, bool yperiodic_, bool zperiodic_, int init_mem);

	void container_draw_particles(container &container, rust::Str filename);
}