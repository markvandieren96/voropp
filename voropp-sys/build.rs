fn main() {
	cxx_build::bridge("src/lib.rs")
		.include("src/voro/src/")
		.file("src/voro/src/voro++.cc")
		.include("src/")
		.file("src/interface.cpp")
		.warnings(false)
		.compile("voropp");
}
