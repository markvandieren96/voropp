use voropp_sys::{cxx, ffi};

pub struct Container {
	obj: cxx::UniquePtr<ffi::container>,
}

impl Container {
	pub fn new(
		ax_: f64,
		bx_: f64,
		ay_: f64,
		by_: f64,
		az_: f64,
		bz_: f64,
		nx_: i32,
		ny_: i32,
		nz_: i32,
		xperiodic_: bool,
		yperiodic_: bool,
		zperiodic_: bool,
		init_mem: i32,
	) -> Self {
		Self {
			obj: ffi::container_ctor(
				ax_, bx_, ay_, by_, az_, bz_, nx_, ny_, nz_, xperiodic_, yperiodic_, zperiodic_,
				init_mem,
			),
		}
	}

	pub fn put(&mut self, n: i32, x: f64, y: f64, z: f64) {
		self.obj.as_mut().unwrap().put(n, x, y, z);
	}

	pub fn sum_cell_volumes(&mut self) -> f64 {
		self.obj.as_mut().unwrap().sum_cell_volumes()
	}

	pub fn find_voronoi_cell(
		&mut self,
		x: f64,
		y: f64,
		z: f64,
		rx: &mut f64,
		ry: &mut f64,
		rz: &mut f64,
		i: &mut i32,
	) -> bool {
		self.obj
			.as_mut()
			.unwrap()
			.find_voronoi_cell(x, y, z, rx, ry, rz, i)
	}

	pub fn draw_particles(&mut self, filename: &str) {
		ffi::container_draw_particles(self.obj.as_mut().unwrap(), filename);
	}
}
