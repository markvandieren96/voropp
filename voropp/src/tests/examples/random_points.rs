use crate::Container;

#[test]
fn test_random_points() {
	// Set up constants for the container geometry
	const X_MIN: f64 = -1.0;
	const X_MAX: f64 = 1.0;
	const Y_MIN: f64 = -1.0;
	const Y_MAX: f64 = 1.0;
	const Z_MIN: f64 = -1.0;
	const Z_MAX: f64 = 1.0;
	const CVOL: f64 = (X_MAX - X_MIN) * (Y_MAX - Y_MIN) * (X_MAX - X_MIN);

	// Set up the number of blocks that the container is divided into
	const N_X: i32 = 6;
	const N_Y: i32 = 6;
	const N_Z: i32 = 6;

	// Set the number of particles that are going to be randomly introduced
	const PARTICLES: i32 = 20;

	// Create a container with the geometry given above, and make it
	// non-periodic in each of the three coordinates. Allocate space for
	// eight particles within each computational block
	let mut container = Container::new(
		X_MIN, X_MAX, Y_MIN, Y_MAX, Z_MIN, Z_MAX, N_X, N_Y, N_Z, false, false, false, 8,
	);

	for i in 0..PARTICLES {
		let x = X_MIN + fastrand::f64() * (X_MAX - X_MIN);
		let y = Y_MIN + fastrand::f64() * (Y_MAX - Y_MIN);
		let z = Z_MIN + fastrand::f64() * (Z_MAX - Z_MIN);
		container.put(i, x, y, z);
	}

	let vvol = container.sum_cell_volumes();

	println!(
		"Container volume: {}\nVoronoi volume: {}\nDifference: {}\n",
		CVOL,
		vvol,
		vvol - CVOL
	);
	assert!((vvol - CVOL).abs() < 0.0001);
}
