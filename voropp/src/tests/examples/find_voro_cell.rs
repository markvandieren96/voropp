use crate::Container;

#[test]
fn test_find_voro_cell() {
	// The sampling distance for the grids of find_voronoi_cell calls
	const H: f64 = 0.05;

	// The cube of the sampling distance, corresponding the amount of volume
	// associated with a sample point
	//const HCUBE: f64 = H * H * H;

	// Set the number of particles that are going to be randomly introduced
	const PARTICLES: i32 = 20;

	// Create a container with the geometry given above, and make it
	// non-periodic in each of the three coordinates. Allocate space for
	// eight particles within each computational block
	let mut container = Container::new(
		0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 5, 5, 5, false, false, false, 8,
	);

	// Randomly add particles into the container
	for i in 0..PARTICLES {
		let x = fastrand::f64();
		let y = fastrand::f64();
		let z = fastrand::f64();
		container.put(i, x, y, z);
	}

	// Output the particle positions in gnuplot format
	//container.draw_particles("find_voro_cell_p.gnu");

	// Create a blank array for storing the sampled Voronoi volumes
	let mut samp_v: [i32; PARTICLES as usize] = [0; PARTICLES as usize];
	for i in 0..PARTICLES as usize {
		samp_v[i] = 0;
	}

	// Scan over a grid covering the entire container, finding which
	// Voronoi cell each point is in, and tallying the result as a method
	// of sampling the volume of each Voronoi cell
	let mut z = 0.5;
	while z < 1.0 {
		let mut y = 0.5;
		while y < 1.0 {
			let mut x = 0.5;
			while x < 1.0 {
				let mut rx = 0.0;
				let mut ry = 0.0;
				let mut rz = 0.0;
				let mut i = 0;

				if container.find_voronoi_cell(x, y, z, &mut rx, &mut ry, &mut rz, &mut i) {
					samp_v[i as usize] += 1;
				}
				x += H;
			}

			y += H;
		}

		z += H;
	}
}
